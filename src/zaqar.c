#include "zaqar.h"

#define MESSAGE "You got mail!!! xo xo xo"

volatile sig_atomic_t sigalrm_flag = false;

int main(void) {
	int in_sock, own_id;
	fd_set active_fd_set, read_fd_set;
	struct sockaddr_in peer_name;
	node_handle_t *neighbors;
	uint16_t num_neighbors = 0;

	own_id = get_own_id();
	if (own_id < 0) {
		perror("Couldn't extract own ID.");
		exit(EXIT_FAILURE);
	}

	// Enables echo broadcast pings
	enable_echo_broadcast();

	// Searches network for neighbors
	search_for_neighbors(&neighbors, &num_neighbors, PORT);

	// Sets a timer and handler to produce interrupts for sending messages
	set_timer_and_handler(handle_alarm, TIMER_INTERVAL);

	// Creates a socket and sets it up to accept connections
	in_sock = create_socket_and_listen(PORT, BACKLOG_SIZE);

	// Initializes the set of active sockets
	// Clears the descriptor set
	FD_ZERO(&active_fd_set);
	// Sets socket in active readset
	FD_SET(in_sock, &active_fd_set);

	while (1) {
		if (sigalrm_flag) {
			// It's time to send a message!
			char new_message[MAX_MESSAGE_LENGTH];

			search_for_neighbors(&neighbors, &num_neighbors, PORT);
			create_message(neighbors, new_message, own_id, num_neighbors, MAX_MESSAGE_LENGTH);

			for (uint8_t i = 0; i < num_neighbors; ++i) {
				if (node_get_status(neighbors[i]) == NODE_PRESENT) {
					send_message(node_get_addr(neighbors[i]), new_message);
				}
			}

			sigalrm_flag = false;
		}

		// Shallow copies the readset
		read_fd_set = active_fd_set;

		// Blocks until input arrives on one or more active sockets
		if (select(FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0) {
			// Handles the wake-up from alarm signal interrupts
			if (sigalrm_flag) {
				continue;
			}

			perror("Couldn't initiate synchronous I/O multiplexing.");
			exit(EXIT_FAILURE);
		}

		// Services all the sockets with input pending
		for (int i = 0; i < FD_SETSIZE; ++i) {
			if (FD_ISSET(i, &read_fd_set)) {
				if (i == in_sock) {
					// Connection request on original socket
					accept_connection(in_sock, &peer_name, &active_fd_set);
				} else { 
					// Data arriving on an already-connected socket
					if (read_from_peer(i, MAX_MESSAGE_LENGTH) < 0) {
						close(i);
						FD_CLR(i, &active_fd_set);
					}
				}
			}
		}
	}

	return 0;
}

void handle_alarm(int sig) {
	if (sig != SIGALRM) {
		return;
	} else {
		sigalrm_flag = true;
	}
}