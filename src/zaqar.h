#ifndef ZAQAR_H_
#define ZAQAR_H_

#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/select.h>

#include <sys/signalfd.h>

#include "helpers.h"
#include "node.h"

#define TIMER_INTERVAL 10
#define PORT 5000
#define MAX_MESSAGE_LENGTH 277
#define BACKLOG_SIZE 2

void handle_alarm(int sig);

#endif //ZAQAR_H_
