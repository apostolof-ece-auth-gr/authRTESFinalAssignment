#include <stdlib.h>
#include "unity.h"

#include "node.h"

struct node_t {
	struct sockaddr_in addr;
	uint64_t** events;
	uint64_t appearance_duration;
	uint8_t events_size;
	node_status _node_status;
};

void test_node_init(void) {
	struct sockaddr_in myaddr;

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(2288);
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	node_handle_t node = node_init(myaddr);
	TEST_ASSERT_NOT_NULL(node);

	node_free(node);
}

void test_node_add_timestamp(void) {
	struct sockaddr_in myaddr;

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(2288);
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	node_handle_t node = node_init(myaddr);

	time_t base_time = time(NULL);

	node_add_timestamp(node, base_time - 15, true);
	TEST_ASSERT_EQUAL_MEMORY(base_time - 15, node->events[0][0], sizeof(time_t));
	TEST_ASSERT_EQUAL_MEMORY(0, node->events[1][0], sizeof(time_t));

	node_add_timestamp(node, base_time - 10, false);
	TEST_ASSERT_EQUAL_MEMORY(base_time - 10, node->events[1][0], sizeof(time_t));

	node_add_timestamp(node, base_time - 5, true);
	TEST_ASSERT_EQUAL_MEMORY(base_time - 5, node->events[0][1], sizeof(time_t));
	TEST_ASSERT_EQUAL_MEMORY(0, node->events[1][1], sizeof(time_t));

	node_add_timestamp(node, base_time, false);
	TEST_ASSERT_EQUAL_MEMORY(base_time, node->events[1][1], sizeof(time_t));

	node_add_timestamp(node, base_time + 5, false);
	TEST_ASSERT_EQUAL_MEMORY(base_time, node->events[1][1], sizeof(time_t));

	node_add_timestamp(node, base_time + 10, true);
	TEST_ASSERT_EQUAL_MEMORY(base_time + 10, node->events[0][2], sizeof(time_t));
	TEST_ASSERT_EQUAL_MEMORY(0, node->events[1][2], sizeof(time_t));

	node_free(node);
}

void test_node_get_addr(void){
	struct sockaddr_in myaddr;

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(2288);
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	node_handle_t node = node_init(myaddr);

	struct sockaddr_in actual = node_get_addr(node);
	TEST_ASSERT_EQUAL_MEMORY(&myaddr, &actual, sizeof(myaddr));

	node_free(node);
}

void test_node_get_status(void){
	struct sockaddr_in myaddr;

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(2288);
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	node_handle_t node = node_init(myaddr);

	TEST_ASSERT_EQUAL_INT(NODE_INITIALIAZED, node_get_status(node));

	node_add_timestamp(node, time(NULL), true);
	TEST_ASSERT_EQUAL_INT(NODE_PRESENT, node_get_status(node));

	node_add_timestamp(node, time(NULL), false);
	TEST_ASSERT_EQUAL_INT(NODE_GONE, node_get_status(node));

	node_free(node);
}

void test_node_get_latest_appearance_duration(void){
	struct sockaddr_in myaddr;

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(2288);
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	node_handle_t node = node_init(myaddr);

	time_t base_time = time(NULL);
	node_add_timestamp(node, base_time - 10, true);
	TEST_ASSERT_UINT8_WITHIN(1, 10, node_get_latest_appearance_duration(node));

	node_add_timestamp(node, base_time + 10, false);
	TEST_ASSERT_EQUAL_UINT8(20, node_get_latest_appearance_duration(node));

	node_free(node);
}

void test_node_get_total_appearance_duration(void){
	struct sockaddr_in myaddr;

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(2288);
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	node_handle_t node = node_init(myaddr);

	time_t base_time = time(NULL);

	node_add_timestamp(node, base_time - 30, true);
	node_add_timestamp(node, base_time - 25, false);
	TEST_ASSERT_EQUAL_UINT8(5, node_get_total_appearance_duration(node));

	node_add_timestamp(node, base_time - 12, true);
	node_add_timestamp(node, base_time - 5, false);
	TEST_ASSERT_EQUAL_UINT8(12, node_get_total_appearance_duration(node));

	node_free(node);
}

void test_node_get_event_table(void){
	struct sockaddr_in myaddr;

	myaddr.sin_family = AF_INET;
	myaddr.sin_port = htons(2288);
	myaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	node_handle_t node = node_init(myaddr);

	time_t base_time = time(NULL);

	node_add_timestamp(node, base_time - 15, true);
	node_add_timestamp(node, base_time - 10, false);
	node_add_timestamp(node, base_time - 5, true);
	node_add_timestamp(node, base_time, false);
	node_add_timestamp(node, base_time + 5, false);
	node_add_timestamp(node, base_time + 10, true);

	time_t** event_table;
	uint8_t num_entries = node_get_event_table(node, &event_table);
	TEST_ASSERT_EQUAL_UINT8(3, num_entries);
	TEST_ASSERT_NOT_NULL(event_table);

	time_t** expected_table = (time_t**) malloc(2 * sizeof(time_t *));
	expected_table[0] = (time_t*) malloc(3 * sizeof(time_t));
	expected_table[1] = (time_t*) malloc(3 * sizeof(time_t));

	expected_table[0][0] = base_time - 15;
	expected_table[1][0] = base_time - 10;
	expected_table[0][1] = base_time - 5;
	expected_table[1][1] = base_time;
	expected_table[0][2] = base_time + 10;
	expected_table[1][2] = 0;

	TEST_ASSERT_EQUAL_MEMORY_ARRAY(expected_table[0], event_table[0], sizeof(time_t), 3);
	TEST_ASSERT_EQUAL_MEMORY_ARRAY(expected_table[1], event_table[1], sizeof(time_t), 3);

	node_free(node);
}
