#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "unity.h"
#include "circ_buff.h"

#define BUFFER_SIZE 8
#define BUFFER_ELEMENT_SIZE 278

int compar(const void* entry1, const void* entry2) {
	char **pstr1 = (char**)entry1, **pstr2 = (char**)entry2;
	char *str1 = calloc(strlen(*pstr1) + 1, sizeof(char)),
		*str2 = calloc(strlen(*pstr2) + 1, sizeof(char));
	strcpy(str1, *pstr1);
	strcpy(str2, *pstr2);

	const char s[2] = "_";
	strtok(str1, s);
	char* rest = strtok(NULL, "");
	int num1 = atoi(rest);

	strtok(str2, s);
	rest = strtok(NULL, "");
	int num2 = atoi(rest);

	free(str1);
	free(str2);

	return num1 - num2;
}

void free_buffer(char** buffer) {
	for(uint8_t i = 0; i < BUFFER_SIZE; ++i) {
		free(buffer[i]);
	}
}

void test_circ_buf_init(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	TEST_ASSERT_NOT_NULL(circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE));

	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_put(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);
	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i);

		circ_buf_put(circ_buf, temp);
	}

	TEST_ASSERT_EQUAL_STRING_LEN("string_0", buffer[0], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_1", buffer[1], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_2", buffer[2], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_3", buffer[3], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_4", buffer[4], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_5", buffer[5], BUFFER_ELEMENT_SIZE);

	for(uint8_t i = 0; i < BUFFER_SIZE; ++i) {
		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i);

		circ_buf_put(circ_buf, temp);
	}

	free(temp);

	TEST_ASSERT_EQUAL_STRING_LEN("string_2", buffer[0], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_3", buffer[1], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_4", buffer[2], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_5", buffer[3], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_6", buffer[4], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_7", buffer[5], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_0", buffer[6], BUFFER_ELEMENT_SIZE);
	TEST_ASSERT_EQUAL_STRING_LEN("string_1", buffer[7], BUFFER_ELEMENT_SIZE);

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_mul_add(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);
	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	char** to_add = (char **) malloc(5 * sizeof(char*));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i);

		to_add[i] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

		if (i < 5) {
			snprintf(to_add[i], BUFFER_ELEMENT_SIZE, "string_%d", i + 10);
		}

		circ_buf_put(circ_buf, temp);
	}

	free(temp);

	circ_buf_mul_add(circ_buf, to_add, 5, compar);

	char* actual = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_3", actual, BUFFER_ELEMENT_SIZE);
	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_4", actual, BUFFER_ELEMENT_SIZE);
	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_5", actual, BUFFER_ELEMENT_SIZE);
	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_10", actual, BUFFER_ELEMENT_SIZE);
	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_11", actual, BUFFER_ELEMENT_SIZE);
	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_12", actual, BUFFER_ELEMENT_SIZE);
	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_13", actual, BUFFER_ELEMENT_SIZE);
	circ_buf_get(circ_buf, actual);
	TEST_ASSERT_EQUAL_STRING_LEN("string_14", actual, BUFFER_ELEMENT_SIZE);

	free(actual);
	circ_buf_free(circ_buf);
	free(buffer);
}

void test_circ_buf_get(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);
	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i);

		circ_buf_put(circ_buf, temp);
	}

	free(temp);

	char* actual = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	char* expected = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		circ_buf_get(circ_buf, actual);
		snprintf(expected, BUFFER_ELEMENT_SIZE, "string_%d", i);

		TEST_ASSERT_EQUAL_STRING_LEN(expected, actual, BUFFER_ELEMENT_SIZE);
	}

	free(actual);
	free(expected);

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_read(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);
	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i);

		circ_buf_put(circ_buf, temp);
	}

	free(temp);

	char* actual = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	char* expected = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		circ_buf_read(circ_buf, i, actual);
		snprintf(expected, BUFFER_ELEMENT_SIZE, "string_%d", i);

		TEST_ASSERT_EQUAL_STRING_LEN(expected, actual, BUFFER_ELEMENT_SIZE);
		TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE - 2, circ_buf_size(circ_buf));
	}

	free(actual);
	free(expected);

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_serialize(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);
	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i);

		circ_buf_put(circ_buf, temp);
	}

	free(temp);

	char* serialized;
	uint8_t serialized_length = circ_buf_serialize(circ_buf, &serialized);
	TEST_ASSERT_EQUAL_UINT8(53, serialized_length);

	char* expected = "string_0\rstring_1\rstring_2\rstring_3\rstring_4\rstring_5";
	TEST_ASSERT_EQUAL_STRING_LEN(expected, serialized, 53);

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_deserialize(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	char* serialized = "string_0\rstring_1\rstring_2\rstring_3\rstring_4\rstring_5";
	uint8_t deserialized_size = circ_buf_deserialize(circ_buf, serialized);
	TEST_ASSERT_EQUAL_UINT8(6, deserialized_size);

	char* actual = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	char* expected = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		circ_buf_get(circ_buf, actual);
		snprintf(expected, BUFFER_ELEMENT_SIZE, "string_%d", i);

		TEST_ASSERT_EQUAL_STRING_LEN(expected, actual, BUFFER_ELEMENT_SIZE);
	}

	free(actual);
	free(expected);

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_empty(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	TEST_ASSERT_TRUE(circ_buf_empty(circ_buf));

	for(uint8_t i = 0; i < BUFFER_SIZE; ++i) {
		circ_buf_put(circ_buf, "Lorem ipsum");
	}

	TEST_ASSERT_FALSE(circ_buf_empty(circ_buf));

	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < BUFFER_SIZE; ++i) {
		circ_buf_get(circ_buf, temp);
	}

	free(temp);

	TEST_ASSERT_TRUE(circ_buf_empty(circ_buf));

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}


void test_circ_buf_full(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	for(uint8_t i = 0; i < (BUFFER_SIZE - 1); ++i) {
		circ_buf_put(circ_buf, "Lorem ipsum");
		TEST_ASSERT_FALSE(circ_buf_full(circ_buf));
	}

	circ_buf_put(circ_buf, "Lorem ipsum");
	TEST_ASSERT_TRUE(circ_buf_full(circ_buf));

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}


void test_circ_buf_capacity(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE, circ_buf_capacity(circ_buf));

	circ_buf_put(circ_buf, "Lorem ipsum");
	circ_buf_put(circ_buf, "Lorem ipsum");

	TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE, circ_buf_capacity(circ_buf));

	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	circ_buf_get(circ_buf, temp);
	free(temp);

	TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE, circ_buf_capacity(circ_buf));

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_size(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	TEST_ASSERT_EQUAL_INT8(0, circ_buf_size(circ_buf));

	for(uint8_t i = 0; i < BUFFER_SIZE; ++i) {
		circ_buf_put(circ_buf, "Lorem ipsum");
		TEST_ASSERT_EQUAL_INT8(i + 1, circ_buf_size(circ_buf));
	}

	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));

	for(uint8_t i = 0; i < BUFFER_SIZE; ++i) {
		circ_buf_get(circ_buf, temp);
		TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE - i - 1, circ_buf_size(circ_buf));
	}

	free(temp);

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_reset(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	for(uint8_t i = 0; i < BUFFER_SIZE - 3; ++i) {
		circ_buf_put(circ_buf, "Lorem ipsum");
	}

	circ_buf_reset(circ_buf);

	TEST_ASSERT_EQUAL_INT8(0, circ_buf_size(circ_buf));
	TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE, circ_buf_capacity(circ_buf));
	TEST_ASSERT_TRUE(circ_buf_empty(circ_buf));
	TEST_ASSERT_FALSE(circ_buf_full(circ_buf));

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_circ_buf_element_size(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	TEST_ASSERT_EQUAL_INT8(BUFFER_ELEMENT_SIZE, circ_buf_element_size(circ_buf));

	circ_buf_put(circ_buf, "Lorem ipsum");
	circ_buf_put(circ_buf, "Lorem ipsum");

	TEST_ASSERT_EQUAL_INT8(BUFFER_ELEMENT_SIZE, circ_buf_element_size(circ_buf));

	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	circ_buf_get(circ_buf, temp);
	free(temp);

	TEST_ASSERT_EQUAL_INT8(BUFFER_ELEMENT_SIZE, circ_buf_element_size(circ_buf));

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_advance_pointer(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	advance_pointer(circ_buf);
	TEST_ASSERT_EQUAL_INT8(1, circ_buf_size(circ_buf));
	advance_pointer(circ_buf);
	TEST_ASSERT_EQUAL_INT8(2, circ_buf_size(circ_buf));

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_retreat_pointer(void) {
	char** buffer = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf = circ_buf_init(buffer, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	for(uint8_t i = 0; i < BUFFER_SIZE - 3; ++i) {
		circ_buf_put(circ_buf, "Lorem ipsum");
	}

	retreat_pointer(circ_buf);
	TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE - 4, circ_buf_size(circ_buf));
	retreat_pointer(circ_buf);
	TEST_ASSERT_EQUAL_INT8(BUFFER_SIZE - 5, circ_buf_size(circ_buf));

	circ_buf_free(circ_buf);
	free_buffer(buffer);
	free(buffer);
}

void test_diff_bufs(void) {
	char** buffer1 = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	char** buffer2 = (char **) malloc(BUFFER_SIZE * sizeof(char *));
	for (uint8_t buff_el = 0; buff_el < BUFFER_SIZE; ++buff_el) {
		buffer1[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
		buffer2[buff_el] = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	}

	cbuf_handle_t circ_buf1 = circ_buf_init(buffer1, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);
	cbuf_handle_t circ_buf2 = circ_buf_init(buffer2, BUFFER_SIZE, BUFFER_ELEMENT_SIZE);

	char* temp = (char *) malloc(BUFFER_ELEMENT_SIZE * sizeof(char));
	for(uint8_t i = 0; i < (BUFFER_SIZE - 2); ++i) {
		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i);
		circ_buf_put(circ_buf1, temp);

		snprintf(temp, BUFFER_ELEMENT_SIZE, "string_%d", i + 2);
		circ_buf_put(circ_buf2, temp);
	}

	free(temp);

	char** add1 = NULL;
	char** add2 = NULL;
	diff_bufs(circ_buf1, circ_buf2, &add1, &add2);

	TEST_ASSERT_NOT_NULL(add1[0]);
	TEST_ASSERT_NOT_NULL(add2[0]);

	TEST_ASSERT_EQUAL_STRING("string_6", add1[0]);
	TEST_ASSERT_EQUAL_STRING("string_7", add1[1]);
	TEST_ASSERT_EQUAL_STRING(EOB, add1[2]);
	TEST_ASSERT_EQUAL_STRING("string_0", add2[0]);
	TEST_ASSERT_EQUAL_STRING("string_1", add2[1]);
	TEST_ASSERT_EQUAL_STRING(EOB, add2[2]);

	circ_buf_free(circ_buf1);
	free_buffer(buffer1);
	free(buffer1);
	circ_buf_free(circ_buf2);
	free_buffer(buffer2);
	free(buffer2);
}
