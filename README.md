# Real Time and Embedded Systems Final Assignment, 2019, AUTH
> Distributed messaging using embedded, real time systems

This is an *experimental* application developed as part of the course "Real Time and Embedded Systems" assignment, that took place in the Department of Electrical & Computer Engineering at Aristotle University of Thessaloniki in 2018-2019.

The goal is to develop and test a distributed messaging application based on an embedded, real time system. To this end, Raspberry PIs were used as the embedded, RT system and a C application was developed and cross-compiled to be executed on the raspberries. More information and test results are presented in the report pdf.

---

## Prerequisites

This project was developed for execution on Raspberry Pi Zero W. The rabian image used during development and testing can be found in the repository [`./rasbian_image/`](https://gitlab.com/apostolof-ece-auth-gr/authRTESFinalAssignment/tree/master/rasbian_image) directory. Installation directions can be found in Raspberry's website [here](https://www.raspberrypi.org/documentation/installation/installing-images/linux.md).

There are two ways of connecting to the system. By connecting to the port denoted "USB", a new network interface will appear, Raspberry's IP is 192.168.0.1/24 in that network, so you need to configure a static IP for your PC in the range 192.168.0.[2-254].

The device also creates an ad-hoc WiFi network with SSID `piZero-ESPX` where it's IP is 10.0.0.1/8. If you want to connect through this WiFi network you need to configure a static IP for your PC in the range 10.[0-255].[0-255].[0-254].

The only user in the system is `root` with password `espx-2019`. You can connect using the command:
```sh
ssh 192.168.0.1 -lroot
```
and typing the password when connecting through wire, or the command:
```sh
ssh 10.0.0.1 -lroot
```
when connecting using the WiFi.

If you plan to test using multiple Raspberries, each one must have a different IP. To change the IP used by a node modify the `/etc/network/interfaces` file appropriately.

In order to compile the project's code you need to install and properly configure the gcc cross-compiler found [here](https://github.com/abhiTronix/raspberry-pi-cross-compilers), instructions are available on their [wiki](https://github.com/abhiTronix/raspberry-pi-cross-compilers/wiki).

---

## Execution

To execute the code, you first need to compile it using:
```sh
make
```

Then you need to copy the code over to the Raspberry using the command:
```sh
scp zaqar root@<Raspberry IP>:~/
```

You can either execute the program manually using the commands:
```sh
ssh <Raspberry IP> -lroot
./zaqar
```
or follow the instructions [here](https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/) to make the program run at startup.

---

## Status

As of the completion of the project, it will NOT be maintained. The aim is to ensure its originality to the day it was delivered. By no means should it ever be considered stable or safe to use, as it may contain incomplete parts, critical bugs and security vulnerabilities.

---

## Support

Reach out to me:

- [apostolof's email](mailto:apotwohd@gmail.com "apotwohd@gmail.com")

---

## License

[![Beerware License](https://img.shields.io/badge/license-beerware%20%F0%9F%8D%BA-blue.svg)](https://gitlab.com/apostolof-ece-auth-gr/authRTESFinalAssignment/blob/master/LICENSE.md)

---
<sub>In Mesopotamian mythology, Zaqar is the messenger of the god Sin. He relays these messages to mortals through his power over their dreams and nightmares. </sub>