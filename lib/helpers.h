#ifndef HELPERS_H_
#define HELPERS_H_

#define _GNU_SOURCE

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <ifaddrs.h>
#include <linux/if_link.h>
#include <sys/time.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>

#include "node.h"

// Macros to turn a numeric macro into a string literal
#define xstr(s) str(s)
#define str(s) #s

// Neighbor discovery related definitions
#define ARP_CACHE "/proc/net/arp"
#define ARP_STRING_LEN 1023
#define ARP_BUFFER_LEN (ARP_STRING_LEN + 1)
#define PING_BUFFER_LEN 1024

// Format for fscanf() to read the 1st field of ARP
#define ARP_LINE_FORMAT "%" xstr(ARP_STRING_LEN) "s %*s %*s %*s %*s %*s"

int get_own_id(void);

int extract_id_from_ip(const char *ip);

void set_timer_and_handler(void (*handler)(int), long int timer_interval);

void enable_echo_broadcast(void);

void search_for_neighbors(node_handle_t **neighbors, uint16_t *num_neighbors, uint16_t port);

void create_message(node_handle_t *neighbors, char *new_message, int own_id,
	uint8_t num_neighbors, uint16_t max_message_length);

bool check_node_alive(const char *ipv4);

int create_socket_and_listen(uint16_t port, uint8_t backlog_size);

void send_message(struct sockaddr_in peer_name, const char *message);

void accept_connection(int sock, struct sockaddr_in *client_name, fd_set *active_fd_set);

void write_to_peer(int filedes, const char *message);

int read_from_peer(int file_des, uint16_t max_line);

void init_sockaddr(struct sockaddr_in *name, const char *hostname, uint16_t port);

#endif //HELPERS_H_
