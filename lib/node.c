#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "node.h"

// Defines the node structure
struct node_t {
	struct sockaddr_in addr;
	uint64_t **events;
	uint64_t appearance_duration;
	uint8_t events_size;
	node_status _node_status;
};

node_handle_t node_init(struct sockaddr_in addr) {
	//assert(addr);

	node_handle_t node = malloc(sizeof(node_t));
	assert(node);

	node->addr = addr;
	node->events_size = 0;
	node->events = (uint64_t **) malloc(2 * sizeof(uint64_t));
	node->events[0] = (uint64_t *) malloc(sizeof(uint64_t));
	node->events[1] = (uint64_t *) malloc(sizeof(uint64_t));
	node->events[0][0] = 0;
	node->events[1][0] = 0;
	node->appearance_duration = 0;
	node->_node_status = NODE_INITIALIAZED;

	return node;
}

void node_free(node_handle_t node) {
	assert(node);

	free(node->events[0]);
	free(node->events[1]);
	free(node->events);
	free(node);
}

void node_add_timestamp(node_handle_t node, time_t timestamp, bool visible) {
	assert(node && timestamp);

	if ((visible && !node->events[1][node->events_size - 1]) ||
		(!visible && node->events[1][node->events_size - 1])) {
		return;
	}

	if (visible) {
		int *realloc_r = realloc(node->events[0], node->events_size + 1);
		if (!realloc_r) {
			node_free(node);
			perror("Error trying to reallocate memory for event timestamps!");
			exit(EXIT_FAILURE);
		}
		realloc_r = realloc(node->events[1], node->events_size + 1);
		if (!realloc_r) {
			node_free(node);
			perror("Error trying to reallocate memory for event timestamps!");
			exit(EXIT_FAILURE);
		}

		node->events[0][node->events_size] = timestamp;
		node->events[1][node->events_size] = 0;
		node->_node_status = NODE_PRESENT;
		++node->events_size;
	} else {
		node->events[1][node->events_size - 1] = timestamp;
		node->_node_status = NODE_GONE;
		node->appearance_duration += node->events[1][node->events_size - 1] -
			node->events[0][node->events_size - 1];
	}
}

struct sockaddr_in node_get_addr(node_handle_t node) {
	assert(node);

	return node->addr;
}

enum node_status node_get_status(node_handle_t node) {
	assert(node);

	return node->_node_status;
}

uint8_t node_get_latest_appearance_duration(node_handle_t node) {
	assert(node);

	if (node->_node_status == NODE_INITIALIAZED) {
		return 0;
	} else if (node->events[1][node->events_size - 1] == 0) {
		return (uint64_t)time(NULL) - node->events[0][node->events_size - 1];
	} else {
		return node->events[1][node->events_size - 1] - node->events[0][node->events_size - 1];
	}
}

uint8_t node_get_total_appearance_duration(node_handle_t node) {
	assert(node);

	return node->appearance_duration;
}

uint8_t node_get_event_table(node_handle_t node, time_t ***event_table) {
	assert(node && event_table);

	if (node->events_size < 1) {
		return 0;
	}

	(*event_table) = (time_t **) malloc(2 * sizeof(time_t *));
	(*event_table)[0] = (time_t *) malloc(node->events_size * sizeof(time_t));
	(*event_table)[1] = (time_t *) malloc(node->events_size * sizeof(time_t));

	memcpy((*event_table)[0], node->events[0], node->events_size * sizeof(time_t));
	memcpy((*event_table)[1], node->events[1], node->events_size * sizeof(time_t));

	return node->events_size;
}