/*
 * Implementation of a simple circular buffer data structure.
 * Based on the example and guide found here:
 * https://embeddedartistry.com/blog/2017/4/6/circular-buffers-in-cc
*/
#ifndef CIRC_BUFF_H_
#define CIRC_BUFF_H_

#include <stdbool.h>

#define EOB "-1"

// Circular buffer structure
typedef struct circ_buf_t circ_buf_t;
// and handle type
typedef circ_buf_t *cbuf_handle_t;

#ifdef TEST //This is a test build
// Makes private functions reachable by the tester
#define unit_static
unit_static void advance_pointer(cbuf_handle_t cbuf);
unit_static void retreat_pointer(cbuf_handle_t cbuf);
#else
#define unit_static static
#endif

// Initializes a circular buffer structure and returns the circular buffer handle.
// Pass in a storage buffer and size.
cbuf_handle_t circ_buf_init(char **buffer, size_t size, size_t element_size);

// Frees a circular buffer structure. Does not free data buffer!
void circ_buf_free(cbuf_handle_t cbuf);

// Resets the circular buffer to empty, head == tail. Data not cleared!
void circ_buf_reset(cbuf_handle_t cbuf);

// Adds data to the end of the buffer, even if the buffer is full. Old data is overwritten.
void circ_buf_put(cbuf_handle_t cbuf, const char *data);

// Adds multiple entries to the buffer, keeping the data in ascending order according to the
// function compar provided in the parameters. If the buffer is full, smallest data are overwritten.
// Doesn't check for duplicates!
void circ_buf_mul_add(cbuf_handle_t cbuf, char **data, uint8_t size,
	int (*compar)(const void *, const void *));

// Retrieves a value from the buffer.
int circ_buf_get(cbuf_handle_t cbuf, char *data);

// Reads a value from the buffer. Does NOT retrieve, size is not reduced!
int circ_buf_read(cbuf_handle_t cbuf, size_t position, char *data);

// Compares the buffers cbuf1 and cbuf2. Elements present on cbuf1 that do not
// exist on cbuf2 are added to the array add2, elements present on cbuf2 that do
// not exist on cbuf1 are added to the array add1.
// Both buffers must be ordered on the same way!
void diff_bufs(cbuf_handle_t cbuf1, cbuf_handle_t cbuf2,
	char ***add1, char ***add2);

// Serializes the whole buffer to a single string
int circ_buf_serialize(cbuf_handle_t cbuf, char **serialized);

// De-serializes a string to a buffer
int circ_buf_deserialize(cbuf_handle_t cbuf, const char *serialized);

// Checks if the buffer is empty.
bool circ_buf_empty(cbuf_handle_t cbuf);

// Checks if the buffer is full.
bool circ_buf_full(cbuf_handle_t cbuf);

// Returns the capacity of the buffer.
size_t circ_buf_capacity(cbuf_handle_t cbuf);

// Returns the number of elements stored in the buffer.
size_t circ_buf_size(cbuf_handle_t cbuf);

// Returns the size of each element.
size_t circ_buf_element_size(cbuf_handle_t cbuf);

#endif //CIRC_BUFF_H_
