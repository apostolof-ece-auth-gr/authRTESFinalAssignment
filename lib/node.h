#ifndef NODE_H_
#define NODE_H_

#include <stdbool.h>
#include <time.h>
#include <arpa/inet.h>

// Node structure
typedef struct node_t node_t;
// and handle type
typedef node_t *node_handle_t;

typedef enum node_status { NODE_INITIALIAZED, NODE_PRESENT, NODE_GONE } node_status;

#ifdef TEST //This is a test build
// Makes private functions reachable by the tester
#define unit_static
#else
#define unit_static static
#endif

// Initializes a node structure and returns the node handle.
node_handle_t node_init(struct sockaddr_in addr);

// Frees a node structure.
void node_free(node_handle_t node);

// Adds an event timestamp to the node. Either the (re)appearance or the disappearance of the node.
void node_add_timestamp(node_handle_t node, time_t timestamp, bool visible);

// Returns the address of the node
struct sockaddr_in node_get_addr(node_handle_t node);

node_status node_get_status(node_handle_t node);

//uint8_t node_get_appear_count(node_handle_t node);

//uint8_t node_get_disappear_count(node_handle_t node);

//void node_get_latest_appear(node_handle_t node, );

//void node_get_latest_disappear(node_handle_t node, );

// Returns the duration (in seconds) of the latest stretch of time that this node has been visible.
uint8_t node_get_latest_appearance_duration(node_handle_t node);

// Returns the total duration (in seconds) of time that this node was visible.
uint8_t node_get_total_appearance_duration(node_handle_t node);

// Returns the event timestamps table for this node.
uint8_t node_get_event_table(node_handle_t node, time_t ***event_table);

// Serializes the whole node to a single string
//int circ_buf_serialize(node_handle_t node, char **serialized);

// De-serializes a string to a node
//int circ_buf_deserialize(node_handle_t node, const char *serialized);

#endif //NODE_H_
