ifeq ($(OS),Windows_NT)
	ifeq ($(shell uname -s),) # not in a bash-like shell
		CLEANUP = del /F /Q
		MKDIR = mkdir
	else # in a bash-like shell, like msys
		CLEANUP = rm -f
		MKDIR = mkdir -p
	endif
		TARGET_EXTENSION=.exe
else
	CLEANUP = rm -f
	MKDIR = mkdir -p
	TARGET_EXTENSION=out
endif

.PHONY: cleandep
.PHONY: clean
.PHONY: test
.PHONY: runners

PATHU = unity/src/
PATHS = src/
PATHT = test/
PATHTR = test/runners/
PATHL = lib/
PATHB = build/
PATHD = build/depends/
PATHO = build/objs/
PATHR = build/results/

BUILD_PATHS = $(PATHB) $(PATHD) $(PATHO) $(PATHR) $(PATHL)

COMPILE = gcc -c
CROSSCOMPILE = arm-linux-gnueabihf-gcc -c

LINK = gcc
CROSSLINK = arm-linux-gnueabihf-gcc

DEPEND = gcc -MM -MG -MF

CFLAGS = -I. -I$(PATHU) -I$(PATHS) -I$(PATHL) -DTEST -DUNITY_OUTPUT_COLOR
CFLAGS += -std=c99
CFLAGS += -Wall
CFLAGS += -Wextra
CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
# CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition

CROSSFLAGS = -I. -I$(PATHS) -I$(PATHL) -D_POSIX_C_SOURCE
CROSSFLAGS += -std=c99
CROSSFLAGS += -march=armv6
CROSSFLAGS += -mfloat-abi=hard
CROSSFLAGS += -mfpu=vfp
CROSSFLAGS += -Wall
CROSSFLAGS += -Wextra
CROSSFLAGS += -Wpointer-arith
CROSSFLAGS += -Wcast-align
# CROSSFLAGS += -Wwrite-strings
CROSSFLAGS += -Wswitch-default
CROSSFLAGS += -Wunreachable-code
CROSSFLAGS += -Winit-self
CROSSFLAGS += -Wmissing-field-initializers
CROSSFLAGS += -Wno-unknown-pragmas
CROSSFLAGS += -Wstrict-prototypes
CROSSFLAGS += -Wundef
CROSSFLAGS += -Wold-style-definition

release: TARCOMPILE = $(CROSSCOMPILE)
test: TARCOMPILE = $(COMPILE)

release: TARLINK = $(CROSSLINK)
test: TARLINK = $(LINK)

release: TARFLAGS = $(CROSSFLAGS)
test: TARFLAGS = $(CFLAGS)

RELEASE = $(PATHB)zaqar.$(TARGET_EXTENSION)
RELEASEDEPS = $(patsubst $(PATHL)%.c,$(PATHO)%.o, $(wildcard $(PATHL)*.c))

release: test $(RELEASEDEPS) $(RELEASE)

RUNNERS = $(patsubst $(PATHT)%.c,$(PATHTR)%_Runner.c, $(wildcard $(PATHT)*.c))

runners: $(RUNNERS) test

RESULTS = $(patsubst $(PATHTR)test_%_Runner.c,$(PATHR)test_%.txt, $(RUNNERS))
DEPS = $(patsubst $(PATHT)test_%.c,$(PATHD)test_%.d, $(wildcard $(PATHT)*.c))

PASSED = `grep -s PASS $(PATHR)*.txt`
FAIL = `grep -s FAIL $(PATHR)*.txt`
IGNORE = `grep -s IGNORE $(PATHR)*.txt`

test: $(BUILD_PATHS) $(RESULTS) $(DEPS)
	@echo "===============UNIT TEST RESULTS===============\n"
	@echo "---------------IGNORES---------------"
	@echo "$(IGNORE)"
	@echo "---------------FAILURES---------------"
	@echo "$(FAIL)"
	@echo "---------------PASSED---------------"
	@echo "$(PASSED)"
	@echo "\n===============DONE===============\n"

$(PATHR)%.txt: $(PATHB)%.$(TARGET_EXTENSION)
	-./$< > $@ 2>&1

$(PATHB)test_%.$(TARGET_EXTENSION): $(PATHO)test_%.o $(PATHO)test_%_Runner.o $(PATHO)%.t.o $(PATHU)unity.o
	$(TARLINK) -o $@ $^

$(PATHO)%.o: $(PATHTR)%.c
	$(TARCOMPILE) $(TARFLAGS) $< -o $@

$(PATHO)%.o:: $(PATHT)%.c
	$(TARCOMPILE) $(TARFLAGS) $< -o $@

$(PATHB)zaqar.$(TARGET_EXTENSION):: $(PATHO)zaqar.o $(RELEASEDEPS)
	$(TARLINK) -o $@ $^

$(PATHO)zaqar.o:: $(PATHS)zaqar.c
	$(TARCOMPILE) $(TARFLAGS) $< -o $@

$(PATHO)%.o:: $(PATHL)%.c
	$(TARCOMPILE) $(TARFLAGS) $< -o $@

$(PATHO)%.t.o:: $(PATHL)%.c
	$(TARCOMPILE) $(TARFLAGS) $< -o $@

$(PATHO)%.o:: $(PATHU)%.c $(PATHU)%.h
	$(TARCOMPILE) $(TARFLAGS) $< -o $@

$(PATHD)%.d:: $(PATHT)%.c
	$(DEPEND) $@ $<

$(PATHTR)%.c:
	ruby unity/auto/generate_test_runner.rb $(patsubst $(PATHTR)%_Runner.c,$(PATHT)%.c, $@) $@

$(PATHB):
	$(MKDIR) $(PATHB)

$(PATHD):
	$(MKDIR) $(PATHD)

$(PATHO):
	$(MKDIR) $(PATHO)

$(PATHR):
	$(MKDIR) $(PATHR)

clean:
	$(CLEANUP) $(PATHO)*.o
	$(CLEANUP) $(PATHB)*.$(TARGET_EXTENSION)
	$(CLEANUP) $(PATHR)*.txt
	$(CLEANUP) $(PATHTR)*.c

cleandep:
	$(CLEANUP) $(PATHD)*.d

.PRECIOUS: $(PATHB)test_%.$(TARGET_EXTENSION)
.PRECIOUS: $(PATHD)%.d
.PRECIOUS: $(PATHO)%.t.o
.PRECIOUS: $(PATHO)%.o
.PRECIOUS: $(PATHR)%.txt
